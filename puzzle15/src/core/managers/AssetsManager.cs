﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace puzzle15.core.managers
{
    class AssetsManager
    {
        public const string Bg = "bg";
        public const string GameButtonPrefix = "gb_";
        public const string VictoryPopup = "victory";

        public const string OkBtn = "okBtn";
        public const string OkBtnDown = "okBtnDown";
        public const string ResetBtn = "resetBtn";
        public const string ResetBtnDown = "resetBtnDown";

        public const string Preview = "preview";


        public SpriteFont Font;

        private const int InfoCount = 8;

        private readonly Info[] _loadData = new Info[InfoCount]
        {
            new Info(Bg),
            new Info(GameButtonPrefix, 1, 15),
            new Info(VictoryPopup),
            new Info(OkBtn),
            new Info(OkBtnDown),
            new Info(ResetBtn),
            new Info(ResetBtnDown),
            new Info(Preview)
        };

        private  List<Texture2D> _textures;

        private readonly ContentManager _content;
    
        private AssetsManager(ContentManager content)
        {
            _content = content;
            _textures = new List<Texture2D>();
        }
        

        public static AssetsManager Instace;
        public static AssetsManager GetInstance(ContentManager content = null)
        {
            return Instace ?? (Instace = new AssetsManager(content));
        }

        public void Load()
        {
            Info info;
            Texture2D texture;

            for (int i = 0; i < InfoCount; i++)
            {
                info = _loadData[i];
                if (info.StartIndex == 0)
                {
                    texture = _content.Load<Texture2D>(info.Name);
                    texture.Name = info.Name;                
                    _textures.Add(texture);
                }
                else
                {
                    for (int j = info.StartIndex; j <= info.LastIndex; j++)
                    {
                        texture = _content.Load<Texture2D>(info.Name + j);
                        texture.Name = info.Name + j;
                        _textures.Add(texture);
                    }
                }
            }

            Font = _content.Load<SpriteFont>("Cooper");
        }

        public Texture2D getTexture(string name)
        {
            for (int i = 0; i <_textures.Count; i++)
            {
                if (_textures[i].Name == name)
                {
                    return _textures[i];
                }
            }

            return null;
        }

        internal class Info
        {
            internal string Name;
            internal int StartIndex;
            internal int LastIndex;

            internal Info(string name, int startIndex = 0, int lastIndex = 0)
            {
                Name = name;
                LastIndex = lastIndex;
                StartIndex = startIndex;
            }
        }
    }
}

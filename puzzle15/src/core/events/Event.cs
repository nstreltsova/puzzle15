﻿namespace puzzle15.core.events
{
    public class Event
    {
        public object Target;
        public object Data;
        public string Name;

        public Event(object data)
        {
            Data = data;
        }
    }
}

﻿using Microsoft.Xna.Framework.Input;

namespace puzzle15.core.events
{
    public class MouseEvent:Event
    {
        public const string MouseDownEvent = "MouseDownEvent";
        public const string MouseUpEvent = "MouseUpEvent";
        public const string MoveEvent = "MoveEvent";

        public readonly MouseState MouseState;

        public MouseEvent(object data, MouseState mouseState):base(data)
        {
            MouseState = mouseState;
        }
    }
}

﻿using System.Collections.Generic;

namespace puzzle15.core.events
{   
    public class EventDispatcher
    {
        public delegate void EventDispatcherDelegate(Event evtData);

        Dictionary<string, List<EventDispatcherDelegate>> m_listeners = new Dictionary<string, List<EventDispatcherDelegate>>();

        public void AddListener(string evtName, EventDispatcherDelegate callback)
        {
            List<EventDispatcherDelegate> evtListeners = null;
            if (m_listeners.TryGetValue(evtName, out evtListeners))
            {

                evtListeners.Remove(callback); //make sure we dont add duplicate
                evtListeners.Add(callback);
            }
            else
            {
                evtListeners = new List<EventDispatcherDelegate>();
                evtListeners.Add(callback);

                m_listeners.Add(evtName, evtListeners);
            }
        }

        public void RemoveListener(string evtName, EventDispatcherDelegate callback)
        {
            List<EventDispatcherDelegate> evtListeners = null;
            if (m_listeners.TryGetValue(evtName, out evtListeners))
            {
                for (int i = 0; i < evtListeners.Count; i++)
                {
                    evtListeners.Remove(callback);
                }
            }
        }

        public void Dispatch(string evtName, Event evt)
        {
            List<EventDispatcherDelegate> evtListeners;
            evt.Target = this;
            evt.Name = evtName;

            if (m_listeners.TryGetValue(evtName, out evtListeners))
            {
                for (int i = 0; i < evtListeners.Count; i++)
                {                    
                    evtListeners[i](evt);
                }
            }
        }
    }
}
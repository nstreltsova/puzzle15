﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using puzzle15.core.events;

namespace puzzle15.core.displayObjects
{
    public class DisplayObject : EventDispatcher
    {      
        private int _x = 0;
        private int _y = 0;
        public int X { get => _x; set => _x = value; }
        public virtual int Y { get => _y; set => _y = value; }

        public float Alpha = 1;

        public float Scale = 1;

        public int Width = 0;
        public int Height = 0;

        private List<DisplayObject> _children = new List<DisplayObject>();

        protected GraphicsDevice _graphics;
        protected SpriteBatch _batch;

        protected DisplayObject _parent;

        public DisplayObject(DisplayObject parent)
        {
            if (parent != null)
            {
                parent.AddChild(this);
            }
        }

        protected DisplayObject() { }

        public DisplayObject GetParent()
        {
            return _parent;
        }

        MouseState previousMouseState;
        Point mousePosition;

        public void AddChild(DisplayObject child)
        {
            AddChildAt(child, _children.Count);
        }

        public void AddChildAt(DisplayObject child, int index)
        {
            int numChildren = _children.Count;

            if (index >= 0 && index <= numChildren)
            {
                if (child._parent == this)
                {
                    SetChildIndex(child, index);
                }
                else
                {
                    child.RemoveFromParent();

                    if (index == numChildren)
                    {
                        _children.Add(child);
                    }
                    else SpliceChildren(index, 0, child);

                    child.SetParent(this);
                }
            }
        }

        public void SetChildIndex(DisplayObject child, int index)
        {
            int oldIndex = GetChildIndex(child);
            if (oldIndex == index) return;

            SpliceChildren(oldIndex, 1);
            SpliceChildren(index, 0, child);
        }

        public DisplayObject RemoveChildAt(int index, bool dispose = false)
        {
            if (index >= 0 && index < _children.Count)
            {
                DisplayObject child = _children[index];

                child.SetParent(null);

                index = _children.IndexOf(child);
                if (index >= 0) SpliceChildren(index, 1);
                if (dispose) child.Dispose();

                return child;
            }

            return null;
        }

        public DisplayObject RemoveChild(DisplayObject child, bool dispose = false)
        {
            int childIndex = GetChildIndex(child);
            if (childIndex != -1) RemoveChildAt(childIndex, dispose);
            return child;
        }

        public int GetChildIndex(DisplayObject child)
        {
            return _children.IndexOf(child);
        }

        public void RemoveFromParent(bool dispose = false)
        {
            if (_parent != null) _parent.RemoveChild(this, dispose);
            else if (dispose) this.Dispose();
        }

        public virtual void Dispose()
        {
            for (int i = 0; i < _children.Count; i++)
            {
                _children[i].Dispose();
            }

            _children = null;
            _batch = null;
            _parent = null;
            _graphics = null;
        }

        public virtual void Update()
        {

            for (int i = 0; i < _children.Count; i++)
            {
                _children[i].Update();
            }

            CheckMouseEvents();
        }

        public virtual bool Contains(Point point)
        {
            return false;
        }

        private void SetParent(DisplayObject value)
        {
            DisplayObject ancestor = value;
            while (ancestor != this && ancestor != null)
                ancestor = ancestor._parent;

            if (ancestor != this)
            {
                _parent = value;
                if (_parent != null)
                {
                    _graphics = _parent._graphics;
                    _batch = new SpriteBatch(_graphics);
                }
            }
        }

        private void SpliceChildren(int startIndex, int deleteCount = int.MaxValue, DisplayObject insertee = null)
        {
            List<DisplayObject> vector = _children;
            int oldLength = vector.Count;

            if (startIndex < 0) startIndex += oldLength;
            if (startIndex < 0) startIndex = 0; else if (startIndex > oldLength) startIndex = oldLength;
            if (startIndex + deleteCount > oldLength) deleteCount = oldLength - startIndex;

            int i;
            int insertCount = insertee != null ? 1 : 0;
            int deltaLength = insertCount - deleteCount;
            int newLength = oldLength + deltaLength;
            int shiftCount = oldLength - startIndex - deleteCount;

            if (deltaLength < 0)
            {
                i = startIndex + insertCount;
                while (shiftCount > 0)
                {
                    vector[i] = vector[(int)(i - deltaLength)];
                    --shiftCount; ++i;
                }
                vector.RemoveRange(newLength, vector.Count - newLength);
            }
            else if (deltaLength > 0)
            {
                i = 1;
                while (shiftCount > 0)
                {
                    vector[(int)(newLength - i)] = vector[(int)(oldLength - i)];
                    --shiftCount; ++i;
                }
                vector.RemoveRange(newLength, vector.Count - newLength);
            }

            if (insertee != null)
                vector[startIndex] = insertee;
        }

        private void CheckMouseEvents()
        {
            MouseState currentMouseState = Mouse.GetState();
            mousePosition = new Point(currentMouseState.X, currentMouseState.Y);

            if (previousMouseState.LeftButton == ButtonState.Released && currentMouseState.LeftButton == ButtonState.Pressed)
            {
                if (Contains(mousePosition))
                {
                    Dispatch(MouseEvent.MouseDownEvent, new MouseEvent(null, currentMouseState));
                }
            }

            if (previousMouseState.LeftButton == ButtonState.Pressed && currentMouseState.LeftButton == ButtonState.Released)
            {
                if (Contains(mousePosition))
                {
                    Dispatch(MouseEvent.MouseUpEvent, new MouseEvent(null, currentMouseState));
                }
            }

            previousMouseState = currentMouseState;
        }
    }
}
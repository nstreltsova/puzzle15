﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace puzzle15.core.displayObjects
{
    class TextField:DisplayObject
    {
        public SpriteFont Font;
        public string Text = "";
        public Color Color = Color.White;

        public TextField(DisplayObject parent) : base(parent)
        {
        }

        public override void Update()
        {
            if (_batch != null && Font != null)
            {
                int x = X;
                int y = Y;
                DisplayObject parent = _parent;
                while (parent != null)
                {
                    x += parent.X;
                    y += parent.Y;
                    parent = parent.GetParent();
                }

                _batch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, Matrix.CreateScale(Scale));
                _batch.DrawString(Font, Text, new Vector2(x / Scale, y / Scale), Color);
                _batch.End();
            }

            base.Update();
        }
    }
}

﻿using Microsoft.Xna.Framework.Graphics;
using puzzle15.core.events;

namespace puzzle15.core.displayObjects
{
    class Button : Sprite
    {
        private Texture2D _defaultTexture;

        public Texture2D DefaultTexture
        {
            get => _defaultTexture;
            set
            {
                _defaultTexture = value;
                Texture = _defaultTexture;

            }
        }

        private bool _isDown = false;

        public Texture2D DownTexture { get; set; }

        public Button(DisplayObject parent) : base(parent)
        {
            Texture = DefaultTexture;

            AddListener(MouseEvent.MouseUpEvent, MouseUpHandler);
            AddListener(MouseEvent.MouseDownEvent, MouseDownHandler);
        }


        public override int Y
        {
            get => _isDown ? base.Y + 2 : base.Y;
            set => base.Y = value;
        }

        private void MouseUpHandler(object obj)
        {
            Texture = DefaultTexture;
            _isDown = false;
        }

        private void MouseDownHandler(object obj)
        {
            Texture = DownTexture;
            _isDown = true;
        }

        public override void Dispose()
        {
            RemoveListener(MouseEvent.MouseUpEvent, MouseUpHandler);
            RemoveListener(MouseEvent.MouseDownEvent, MouseDownHandler);

            Texture = null;
            _defaultTexture = null;
            DownTexture = null;
        }
    }
}

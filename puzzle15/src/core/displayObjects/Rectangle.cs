﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace puzzle15.core.displayObjects
{
    class Rectangle:Sprite
    {
        public Rectangle(DisplayObject parent, int width, int height, Color color) : base(parent)
        {
            Color = color;
            Texture = new Texture2D(_graphics, width, height);
            Color[] colorData = new Color[width * height];

            for (int i = 0; i < colorData.Length; i++)
            {
                colorData[i] = color;
            }

            Texture.SetData(colorData);
        }
    }
}

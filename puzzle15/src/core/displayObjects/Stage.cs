﻿using Microsoft.Xna.Framework.Graphics;

namespace puzzle15.core.displayObjects
{
    class Stage:DisplayObject
    {
        public Stage(GraphicsDevice graphics)
        {
            _graphics = graphics;
            _batch = new SpriteBatch(_graphics);
        }
    }
}

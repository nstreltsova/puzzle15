﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace puzzle15.core.displayObjects
{
    public class Sprite : DisplayObject
    {
        public Sprite(DisplayObject parent) : base(parent)
        {
        }

        public Texture2D Texture;
        protected Color Color = Color.White;

        public override void Update()
        {
            int x = X;
            int y = Y;
            DisplayObject parent = _parent;
            while (parent != null)
            {
                x += parent.X;
                y += parent.Y;
                parent = parent.GetParent();
            }

            if (_batch != null && Texture != null)
            {
                _batch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, Matrix.CreateScale(Scale));
                _batch.Draw(Texture, new Vector2(x / Scale, y / Scale), Color * Alpha);
                _batch.End();
            }

            base.Update();
        }

        public override bool Contains(Point point)
        {
            if (_batch == null || Texture == null)
            {
                return false;
            }

            int x = X;
            int y = Y;
            DisplayObject parent = _parent;
            while (parent != null)
            {
                x += parent.X;
                y += parent.Y;
                parent = parent.GetParent();
            }

            return point.X >= x && point.X <= (x + Texture.Width) && point.Y >= y && point.Y <= (y + Texture.Height);
        }

        public new int Width => _batch != null ? Texture.Width : 0;
        public new int Height => _batch != null ? Texture.Height : 0;

        public override void Dispose()
        {
            Texture = null;          

            base.Dispose();
        }
    }
}

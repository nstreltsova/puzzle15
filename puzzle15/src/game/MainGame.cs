using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using puzzle15.core.displayObjects;
using puzzle15.core.managers;
using puzzle15.game.controller;
using puzzle15.game.view;

namespace puzzle15.game
{
    public class MainGame : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;

        
        Stage stage;
        private GameScreen _gameScreen;

        private GameController _gameController;

        public MainGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 1000;
            graphics.PreferredBackBufferHeight = 700;
        }

        protected override void Initialize()
        { 
            IsMouseVisible = true;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            AssetsManager assetsManaget = AssetsManager.GetInstance(Content);
            assetsManaget.Load();

            stage = new Stage(GraphicsDevice);

            Sprite bg = new Sprite(stage);
            bg.Texture = assetsManaget.getTexture(AssetsManager.Bg);

            _gameController = new GameController();

            _gameScreen = new GameScreen(stage, _gameController);
            _gameScreen.X = 235;
            _gameScreen.Y = 88;                          
        }

        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();  

            _gameController.UpdateTime(gameTime);

            base.Update(gameTime);
        }

        
        protected override void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(Color.CornflowerBlue);

            stage.Update();

            base.Draw(gameTime);
        }
    }
}

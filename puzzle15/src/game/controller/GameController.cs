using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using puzzle15.core.events;
using puzzle15.game.view;

namespace puzzle15.game.controller
{
    public class GameController : EventDispatcher
    {
        public const String WinEvent = "WinEvent";
        public const String TimerEvent = "TimerEvent";
        public const String SwapEvent = "SwapEvent";

        private const int _numsAmountForReshufle = 4;

        public int fieldSize = 4;

        public int PreviousX = 0;
        public int PreviousY = 0;

        private List<int> _randomDataArray;

        private bool _isEndOfGame = false;
        private int _time = 0;
        private int _steps = 0;

        private TimeSpan _interval = new TimeSpan(0, 0, 1);
        private TimeSpan _lastTick = new TimeSpan();

        public GameButton ActiveButton;


        public List<GameButton> fieldVector;

        public GameController()
        {
            fieldVector = new List<GameButton>();
        }

        public List<int> CreatePuzzle()
        {
            _randomDataArray = new List<int>();

            FillNumsArray(fieldSize * fieldSize - 1);

            do
            {
                ShuffleNumsArray(new List<int>());
            } while (!HasArraySolution() || IsNeedReshuffle());

            return _randomDataArray;
        }

        private void FillNumsArray(int size)
        {
            if (_randomDataArray.Count < size)
            {
                _randomDataArray.Add(_randomDataArray.Count + 1);


                FillNumsArray(size);
            }
            else
            {
                _randomDataArray.Add(0);
            }
        }

        private void ShuffleNumsArray(List<int> result)
        {
            Random rnd = new Random();
            int index = rnd.Next(0, _randomDataArray.Count);
            result.Add(_randomDataArray[index]);
            _randomDataArray.RemoveAt(index);

            if (_randomDataArray.Count != 0)
            {

                ShuffleNumsArray(result);
            }
            else
            {
                _randomDataArray = result;
            }
        }

        private bool HasArraySolution()
        {
            int counter = 0;

            for (int i = 0; i < _randomDataArray.Count - 1; i++)
            {
                if (_randomDataArray[i] != 0)
                {
                    for (int j = i + 1; j < _randomDataArray.Count; j++)
                    {
                        if (_randomDataArray[j] != 0)
                        {
                            if (_randomDataArray[i] > _randomDataArray[j])
                            {
                                counter++;
                            }
                        }
                    }
                }
                else
                {
                    counter += (int)(i / fieldSize) + 1;
                }
            }

            return (counter % 2) == 0;
        }

        public void Swap(int index, int x, int y)
        {
            if (ActiveButton == null)
            {
                return;
            }

            if (ActiveButton.Index == index)
            {
                ActiveButton.IsActive = false;
                ActiveButton = null;
                return;
            }

            index = ActiveButton.Index;

            int deltaX = PreviousX - x;
            int deltaY = PreviousY - y;

            if (Math.Abs(deltaX) > Math.Abs(deltaY)) // �������������
            {
                if (deltaX > 0)  // �����
                {
                    SwapItems(index, index - 1);//left
                }
                else
                {
                    SwapItems(index, index + 1);//right
                }
            }
            else
            {
                if (deltaY > 0) // �����
                {
                    SwapItems(index, index - fieldSize); // up
                }
                else
                {
                    SwapItems(index, index + fieldSize);//down
                }
            }

            ActiveButton.IsActive = false;
            ActiveButton = null;
        }

        private void SwapItems(int index1, int index2)
        {
            if (index1 >= 0 && index1 < fieldVector.Count && index2 >= 0 && index2 < fieldVector.Count)
            {
                if (fieldVector[index1].IsEmpty || !fieldVector[index2].IsEmpty)
                {
                    return;
                }

                Texture2D texture = fieldVector[index2].Texture;
                int value = fieldVector[index2].Value;

                fieldVector[index2].Texture = fieldVector[index1].Texture;
                fieldVector[index2].IsEmpty = !fieldVector[index2].IsEmpty;
                fieldVector[index2].Value = fieldVector[index1].Value;

                fieldVector[index1].Texture = texture;
                fieldVector[index1].IsEmpty = !fieldVector[index1].IsEmpty;
                fieldVector[index1].Value = value;

                _steps++;
                Dispatch(SwapEvent, new Event(_steps));

                CheckWin();
            }
        }

        public void ResetField()
        {
            List<int> randomDataArray = CreatePuzzle();

            List<GameButton> tempFieldVector = new List<GameButton>();

            for (int i = 0; i < randomDataArray.Count; i++)
            {
                for (int j = 0; j < fieldVector.Count; j++)
                {
                    if (fieldVector[j].Value == randomDataArray[i])
                    {
                        tempFieldVector.Add(fieldVector[j]);
                        fieldVector.RemoveAt(j);
                        break;
                    }
                }
            }

            fieldVector = tempFieldVector;

            _steps = 0;
            _time = 0;
            _isEndOfGame = false;
        }

        private void CheckWin()
        {
            for (int i = 0; i < fieldVector.Count - 1; i++)
            {
                if (fieldVector[i].Value != i + 1)
                {
                    return;
                }
            }

            Dispatch(WinEvent, new Event(new WinInfo(_time, _steps)));

            _isEndOfGame = true;
        }

        private bool IsNeedReshuffle()
        {
            int prevValue = -1;
            int sequenceAmount = 0;

            for (int i = 0; i < _randomDataArray.Count - 1; i++)
            {
                if (_randomDataArray[i] == 0)
                {
                    continue;
                }

                if (prevValue + 1 == _randomDataArray[i])
                {
                    if (sequenceAmount == _numsAmountForReshufle)
                    {
                        return true;
                    }
                    sequenceAmount++;
                }
                else
                {
                    sequenceAmount = 0;
                }
                    
                prevValue = _randomDataArray[i];
            }

            return false;
        }

        public void UpdateTime(GameTime gameTime)
        {
            if (_isEndOfGame)
            {
                return;
            }

            if (gameTime.TotalGameTime - _lastTick >= _interval)
            {
                _time++;

                _lastTick = gameTime.TotalGameTime;

                Dispatch(TimerEvent, new Event(_time));
            }
        }

        internal class WinInfo
        {
            public int Time;
            public int Steps;

            public WinInfo(int time, int steps)
            {
                Time = time;
                Steps = steps;
            }
        }
    }
}
﻿using Microsoft.Xna.Framework.Graphics;
using puzzle15.core.displayObjects;

namespace puzzle15.game.view
{
    public class GameButton : Sprite
    {
        public int Index;

        private bool isEmpty = false;

        public int Value;

        public bool IsActive = false;

        private bool _scaleDirectionUp = true;

        public bool IsEmpty { get => isEmpty;
            set
            {
                isEmpty = value;
                Alpha = IsEmpty ? 0 : 1;
            }
        }

        public GameButton(DisplayObject parent, int value, Texture2D texture, bool isEmpty = false):base(parent)
        {
            Texture = texture;
            Value = value;
            IsEmpty = isEmpty;
            Alpha = isEmpty ? 0 : 1;
        }

        public override void Update()
        {
            if (IsActive)
            {
                if (_scaleDirectionUp)
                {
                    Scale += 0.001f;
                    if (Scale >= 1.01)
                    {
                        _scaleDirectionUp = false;
                    }
                }
                else
                {
                    Scale -= 0.001f;
                    if (Scale <= 0.99)
                    {
                        _scaleDirectionUp = true;
                    }
                }               
            }

            base.Update();
        }       
    }
}
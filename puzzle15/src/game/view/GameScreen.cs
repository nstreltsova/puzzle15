﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using puzzle15.core.displayObjects;
using puzzle15.core.events;
using puzzle15.core.managers;
using puzzle15.game.controller;

namespace puzzle15.game.view
{


    class GameScreen : Sprite
    {
       
        private TextField _timeTF;
        private TextField _stepsTF;

        private Button _resetBtn;

        private GameController _gameController;

        public GameScreen(DisplayObject parent, GameController gameController):base(parent)
        {
            _gameController = gameController;
            _gameController.AddListener(GameController.TimerEvent, UpdateTime);
            _gameController.AddListener(GameController.SwapEvent, UpdateSteps);

            CreateGameButton();
            
            _gameController.ResetField();
            UpdateButtonsPositions();

            InitTopPanel();
            AddResetButton();
        }

        public void CreateGameButton()
        {

            GameButton button;
            List<GameButton> fieldVector = _gameController.fieldVector;
            int value = -1;
            string textureName;

            for (int i = 0; i < _gameController.fieldSize; i++)
            {
                for (int j = 0; j < _gameController.fieldSize; j++)
                {
                    textureName = AssetsManager.GameButtonPrefix + (value == -1 ? 1 : (value + 1));
                    button = new GameButton(this, (value == -1 ? 0 : value + 1), AssetsManager.GetInstance().getTexture(textureName), value == -1);

                    button.Y = Y + i * (button.Height - 4);
                    button.X = X + (int)(j % _gameController.fieldSize * (button.Width - 1));

                    button.AddListener(MouseEvent.MouseUpEvent, MouseUpHandler);
                    button.AddListener(MouseEvent.MouseDownEvent, MouseDownHandler);

                    fieldVector.Add(button);

                    value++;
                }
            }

            _gameController.AddListener(GameController.WinEvent, WinHandler);
        }

        public void UpdateButtonsPositions()
        {
            List<GameButton> fieldVector = _gameController.fieldVector;
            int index = 0;
            GameButton button;

            for (int i = 0; i < _gameController.fieldSize; i++)
            {
                for (int j = 0; j < _gameController.fieldSize; j++)
                {
                    button = fieldVector[index];

                    button.Y = i * (button.Height - 4);
                    button.X = (int)(j % _gameController.fieldSize * (button.Width - 1));
                    button.Index = index;

                    index++;
                }
            }
        }

        private void WinHandler(Event evt)
        {
            VictoryPopup vp = new VictoryPopup(GetParent(), (GameController.WinInfo)evt.Data);
            vp.AddListener(VictoryPopup.CloseEvent, ResetHandler);
        }

        public void InitTopPanel()
        {
            SpriteFont font = AssetsManager.GetInstance().Font;

            _timeTF = new TextField(this);

            _timeTF.Font = font;
            _timeTF.Text = "0";
            _timeTF.X = 150;
            _timeTF.Y = -90;

            _stepsTF = new TextField(this);

            _stepsTF.Font = font;
            _stepsTF.Text = "0";
            _stepsTF.X = 360;
            _stepsTF.Y = -90;

            Sprite preview = new Sprite(this);
            preview.Texture = AssetsManager.Instace.getTexture(AssetsManager.Preview);
            preview.X = -200;
            preview.Y = 180;
        }

        private void AddResetButton()
        {
            _resetBtn = new Button(this);
            _resetBtn.X = 600;
            _resetBtn.Y = 450;
            _resetBtn.DefaultTexture = AssetsManager.GetInstance().getTexture(AssetsManager.ResetBtn);
            _resetBtn.DownTexture = AssetsManager.GetInstance().getTexture(AssetsManager.ResetBtnDown);
            _resetBtn.AddListener(MouseEvent.MouseUpEvent, ResetHandler);
        }

        private void ResetHandler(Event evt)
        {
            _gameController.ResetField();
            UpdateButtonsPositions();

            _stepsTF.Text = "0";
        }

        private void UpdateTime(Event evt)
        {
            _timeTF.Text = evt.Data.ToString();
        }

        private void UpdateSteps(Event evt)
        {
            _stepsTF.Text = evt.Data.ToString();
        }

        private void MouseDownHandler(Event evt)
        {
            GameButton gb = (GameButton)evt.Target;
            _gameController.ActiveButton = gb;
            _gameController.PreviousX = gb.X;
            _gameController.PreviousY = gb.Y;

            gb.IsActive = true;
        }

        private void MouseUpHandler(Event evt)
        {
            GameButton gb = (GameButton)evt.Target;

            _gameController.Swap(gb.Index, gb.X, gb.Y);
        }
    }




}

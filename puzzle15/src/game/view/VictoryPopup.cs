﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using puzzle15.core.displayObjects;
using puzzle15.core.events;
using puzzle15.core.managers;
using puzzle15.game.controller;
using Rectangle = puzzle15.core.displayObjects.Rectangle;

namespace puzzle15.game.view
{
    class VictoryPopup:Sprite
    {
        public const string CloseEvent = "CloseEvent";

        private GameController.WinInfo _info;

        private TextField _timeTF;
        private TextField _stepsTF;
        private Button _okBtn;
        private Sprite _bg;

        private Rectangle _blackBack;

        public VictoryPopup(DisplayObject parent, GameController.WinInfo info):base(parent)
        {
            _info = info;

            _blackBack = new Rectangle(this, 1000, 700, Color.Black);
            _blackBack.Alpha = 0.5f;

            _bg = new Sprite(this);
            _bg.Texture = AssetsManager.GetInstance().getTexture(AssetsManager.VictoryPopup);
            _bg.X = 50;
            _bg.Y = 160;

            SpriteFont font = AssetsManager.GetInstance().Font;

            _timeTF = new TextField(this);
            _timeTF.Font = font;
            _timeTF.Text = _info.Time.ToString();
            _timeTF.X = 395;
            _timeTF.Y = 370;

            _stepsTF = new TextField(this);
            _stepsTF.Font = font;
            _stepsTF.Text = _info.Steps.ToString();
            _stepsTF.X = 580;
            _stepsTF.Y = 370;

            _okBtn = new Button(this);
            _okBtn.X = 425;
            _okBtn.Y = 470;
            _okBtn.DefaultTexture = AssetsManager.GetInstance().getTexture(AssetsManager.OkBtn);
            _okBtn.DownTexture = AssetsManager.GetInstance().getTexture(AssetsManager.OkBtnDown);
            _okBtn.AddListener(MouseEvent.MouseUpEvent, Close);
        }

        private void Close(object obj)
        {
            RemoveFromParent(true);

            Dispatch(CloseEvent, new Event(null));
        }

        public override void Dispose()
        {
            RemoveListener(MouseEvent.MouseDownEvent, Close);

            _timeTF = null;
            _okBtn = null;
        }
    }
}
